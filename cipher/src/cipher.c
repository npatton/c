#include "cipher.h"

int			main(int ac, char **av)
{
	int		key;

	if (ac == 5)
	{
		key = 0;
		if (av[2][1] == 'c')
		{
			while (*av[4])
				key = (key * 10) + (*(av[4]++) - '0');
			if (av[1][1] == 'd')
				key = 26 - key;
			cipher_caeser(key, av[3]);
		}
		else
			print_error(0);
	}
	else
		print_error(1);
	return (0);
}

void		cipher_caeser(int key, char *str)
{
	if (key > 0 && key < 26)
	{
		while (*str)
		{
			if (caeser_case(*str))
			{
				if (*str >= (caeser_case(*str) - (key - 1)))
					*str = *str - (26 - key);
				else
					*str = *str + key;
			}
			write(1, str++, 1);
		}
	}
	else
		print_error(2);
	write(1, "\n", 1);
}

int			caeser_case(int c)
{
	int ret;

	ret = 0;
	if (c >= 'A' && c <= 'Z')
		ret = 'Z';
	if (c >= 'a' && c <= 'z')
		ret = 'z';
	return (ret);
}

void		print_error(int error)
{
	if (error == 0)
		write(1, "Incorrect cipher type.\n", 23);
	if (error == 1)
	{
		write(1, "Usage:\n$ ./cipher FLAG TYPE STRING KEY\n\n", 40);
		write(1, "Flags:\n-e\tEncode\n-d\tDecode\n\n", 28);
		write(1, "Types:\n-c\tCaeser Cipher\n", 24);
	}
	if (error == 2)
		write(1, "Error: please input a number from 1 to 25", 41);
}
