#ifndef CIPHER_H
# define CIPHER_H
# include <unistd.h>
# include <stdlib.h>

void		cipher_caeser(int key, char *str);
int			caeser_case(int c);
void		print_error(int error);

#endif
