#include "hash_table.h"

static ht_item		ht_new_item(const char *k, const char *v)
{
	ht_item*		i;

	i = malloc(sizeof(ht_item));
	i->key = strdup(k);
	i->value = strdup(v);
	return i;
}

ht_hash_table		*ht_new(void)
{
	ht_hash_table	*ht;

	ht = malloc(sizeof(ht_hash_table));
	ht->size = 53;
	ht->count = 0;
	ht->items = calloc((size_t)ht->size, sizeof(ht_item*));
	return ht;
}

static void			ht_del_item(ht_item *i)
{
	free(i->key);
	free(i->value);
	free(i);
}

void				ht_del_hash_table(ht_hash_table *ht)
{
	int				i;
	ht_item			*item;

	i = 0;
	while (i++ < ht->size)
	{
		item = ht->items[i];
		if (item != NULL)
			ht_del_item(item);
	}
	free(ht->items);
	free(ht);
}
